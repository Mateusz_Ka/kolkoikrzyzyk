import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native'

export default class Field extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isEmpty: true,
      gameFigure: null
    }
  }

  _onPress = () => {
    if (this.state.isEmpty) {
      this.setState({
        isEmpty: false,
        gameFigure: this.props.gameState.currentFigure()
      });
      this.props.gameState.handleFigurePlaced(this.props.row, this.props.column);
    }
  }

  render() {
    return (
      <TouchableHighlight style={styles.field} onPress={this._onPress}>
        <View>
          <Text style={styles.text}>{this.state.gameFigure}</Text>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  field: {
    justifyContent: "center",
    borderColor: 'black',
    borderStyle: 'solid',
    borderWidth: 3,
    height: 120,
    width: 120
  },
  text: {
    textAlign: "center",
    color: "black",
    fontSize: 50,
  }
});
