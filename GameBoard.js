import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Field from './Field';
import GameState from './gameState';
import CurrentPlayer from './CurrentPlayer';

export default class GameBoard extends Component {

  constructor(props) {
    super(props);
    this.gameState = new GameState();
    this.state = { currentPlayer: "x" }
  }

  _updateGameState = (row, column) => {
    this.gameState.figurePlaced(row, column)
    const currentGameState = this.gameState.currentState();
    if (currentGameState.isGameEnded) {
      this.props.endGameHandler(currentGameState.winner ? `Wygrywa ${currentGameState.winner}` : "Remis")
    }
    else {this.setState({ currentPlayer: this.currentFigure() })}
  }
  currentFigure = () => this.gameState.getCurrentFigure();

  render() {
    return (
      <View style={styles.container}>
        <CurrentPlayer currentPlayer={this.state.currentPlayer}></CurrentPlayer>
        <View style={styles.board}>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={0} column={0}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={0} column={1}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={0} column={2}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={1} column={0}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={1} column={1}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={1} column={2}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={2} column={0}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={2} column={1}></Field>
          <Field gameState={{ handleFigurePlaced: this._updateGameState, currentFigure: this.currentFigure }} row={2} column={2}></Field>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  board: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    width: 360,
    height: 360,
    marginTop: 100
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    marginTop: 50
  }
});
