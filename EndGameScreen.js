import React, { Component } from 'react'
import { Text, View, Button, StyleSheet } from 'react-native'

export default class EndGameScreen extends Component {
    render() {
        return (
            <View>
                <Text style={styles.text}>{this.props.result}</Text>
                <Button
                    color={"black"}
                    title={"Zagraj ponownie"}
                    onPress={() => this.props.handlePress()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 40,
        color: "black",
        textAlign: "center",
        marginTop: 80,
        marginBottom: 100
    }
});