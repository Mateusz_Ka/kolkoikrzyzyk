export default class GameState {
    _boardState = [
        [undefined, undefined, undefined],
        [undefined, undefined, undefined],
        [undefined, undefined, undefined],
    ]

    _state = {
        isGameEnded: false,
        winner: undefined
    }
    _moves = 0;

    _figure = "x";

    currentState() { return { ...this._state } }

    figurePlaced(row, column) {
        this._moves++;
        this._boardState[row][column] = this._figure;
        this._calculateState();
        this._figure = this._figure === "x" ? "o" : "x";
    }
    getCurrentFigure() {
        return this._figure;
    }

    _calculateState() {
        if (
            this.are3InRow() ||
            this.are3InColumn() ||
            this.are3InFirstDiagonal() ||
            this.are3InSecondDiagonal()
        ) {
            this.endGame()
        } else if (this._moves === 9) {
            this._state.isGameEnded = true;
        }

    }

    are3InFirstDiagonal() {
        const arr = [this._boardState[0][0], this._boardState[1][1], this._boardState[2][2]];
        if (this._areAllElementsTheSame(arr)) {
            return true;
        }
        return false;
    }

    are3InSecondDiagonal() {
        const arr = [this._boardState[0][2], this._boardState[1][1], this._boardState[2][0]];
        if (this._areAllElementsTheSame(arr)) {
            return true;
        }
        return false;
    }

    are3InColumn() {
        for (let i = 0; i < 3; i++) {
            const arr = [this._boardState[0][i], this._boardState[1][i], this._boardState[2][i]];
            if (this._areAllElementsTheSame(arr)) {
                return true;
            }
        }
        return false
    }

    are3InRow() {
        const winningRow = this._boardState.findIndex(row => this._areAllElementsTheSame(row));
        if (winningRow !== -1) {
            return true;
        }
        return false;
    }

    endGame() {
        this._state.isGameEnded = true;
        this._state.winner = this._figure;
    }

    _areAllElementsTheSame(figuresArray) {
        return figuresArray.every((field, i, arr) => field !== undefined && field === arr[0])
    }
}
