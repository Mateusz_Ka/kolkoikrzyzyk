import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class CurrentPlayer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Aktualny Gracz: {this.props.currentPlayer}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 40,
        color: "black",
        textAlign: "center",
        marginTop: 80,
        marginBottom: 100
    },
    container: {
        height: 60,
        marginTop: 30
    }
});
