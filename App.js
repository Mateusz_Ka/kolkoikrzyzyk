import React, { Component } from 'react';
import { View } from 'react-native';
import WelcomeScreen from './WelcomeScreen';
import GameBoard from './GameBoard';
import EndGameScreen from './EndGameScreen';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: (<WelcomeScreen handlePress={this._startGame}></WelcomeScreen>)
    }
  }

  _startGame = () => {
    this.setState({ screen: <GameBoard endGameHandler={this._endGame}></GameBoard> })
  }

  _endGame = (result) => {
    this.setState({ screen: <EndGameScreen result={result} handlePress={this._startGame}></EndGameScreen> })
  }

  render() {
    return (
      <View>
        {this.state.screen}
      </View>
    );
  }
}